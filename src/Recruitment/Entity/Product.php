<?php

namespace Recruitment\Entity;

use Recruitment\Entity\Exception\InvalidTaxException;
use Recruitment\Entity\Exception\InvalidUnitPriceException;

/**
 * Class Product
 * @package Recruitment\Entity
 */
class Product
{
    /** @var int */
    private $id;

    /** @var float */
    private $price;

    /** @var int */
    private $minQuantity = 1;

    /** @var string */
    private $name;

    /** @var int */
    private $tax = 0;

    /** @var array */
    private $taxValues = [0, 5, 8, 23];

    /**
     * Set price (english notation without thousands separator)
     *
     * @param string $price
     * @return Product
     * @throws InvalidUnitPriceException
     */
    public function setUnitPrice($price): Product
    {
        if (is_int($price) && $price > 0) {
            $this->price = number_format((float)$price, 2, '.', '');
        } else {
            throw new InvalidUnitPriceException();
        }

        return $this;
    }

    /**
     * Set minimum quantity
     *
     * @param  int $quantity
     * @return Product
     */
    public function setMinimumQuantity(int $quantity): Product
    {
        if ($quantity > 0) {
            $this->minQuantity = $quantity;
        } else {
            throw new \InvalidArgumentException;
        }

        return $this;
    }

    /**
     * Set id
     *
     * @param  string $id
     * @return Product
     */
    public function setId($id): Product
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * Get product
     *
     * @return Product
     */
    public function getProduct(): Product
    {
        return $this;
    }

    /**
     * Get minimum quantity
     *
     * @return int
     */
    public function getMinimumQuantity(): int
    {
        return $this->minQuantity;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * Get tax
     *
     * @return int
     */
    public function getTax(): int
    {
        return $this->tax;
    }

    /**
     * Get tax percentage
     *
     * @return string
     */
    public function getTaxPercentage(): string
    {
        return sprintf('%s%%', $this->tax);
    }

    /**
     * Get price gross
     *
     * @return float
     */
    public function getPriceGross(): float
    {
        return ($this->tax / 100 + 1 ) * $this->price;
    }

    /**
     * @param int $tax
     * @return Product
     * @throws InvalidTaxException
     */
    public function setTax(int $tax): Product
    {
        if (in_array($tax, $this->taxValues, true) === false) {
            throw new InvalidTaxException();
        }

        $this->tax = $tax;

        return $this;
    }
}
