<?php

namespace Recruitment\Entity;

use Recruitment\Cart\Cart;

/**
 * Class Order
 * @package Recruitment\Entity
 */
class Order
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var Cart
     */
    private $cart;

    /**
     * Order constructor.
     * @param int $id
     * @param Cart $cart
     */
    public function __construct(int $id, Cart $cart)
    {
        $this->id = $id;
        $this->cart = $cart;
    }

    /**
     * @return array
     */
    public function getDataForView(): array
    {
        return [
            'id' => $this->id,
            'items' => $this->cart->toArray(),
            'total_price' => $this->cart->getTotalPrice(),
            'total_price_gross' => $this->cart->getTotalPriceGross()
        ];
    }
}
