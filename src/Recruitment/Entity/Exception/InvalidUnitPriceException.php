<?php

namespace Recruitment\Entity\Exception;

/**
 * Class InvalidUnitPriceException
 * @package Recruitment\Entity\Exception
 */
class InvalidUnitPriceException extends \Exception
{

}
