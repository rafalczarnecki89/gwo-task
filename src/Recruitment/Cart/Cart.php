<?php

declare(strict_types=1);

namespace Recruitment\Cart;

use Recruitment\Entity\Order;
use Recruitment\Entity\Product;

/**
 * Class Cart
 * @package Recruitment\Cart
 */
class Cart
{
    /** @var array */
    protected $items = [];

    /**
     * Add cart product
     *
     * @param Product $product
     * @param int $quantity
     * @return Cart
     * @throws Exception\QuantityTooLowException
     */
    public function addProduct(Product $product, int $quantity = 1): Cart
    {
        $isAlreadyAdded = false;

        if (count($this->items) > 0) {
            $items = $this->items;
            $this->items = [];

            /** @var Item $item */
            foreach ($items as $key => $item) {
                if ($item->getProduct()->getId() === $product->getId()) {
                    $newQuantity = $quantity + $item->getQuantity();
                    $this->items[] = new Item($item->getProduct(), $newQuantity);
                    $isAlreadyAdded = true;
                } else {
                    $this->items[] = new Item($item->getProduct(), $item->getQuantity());
                }
            }
        }

        if ($isAlreadyAdded === false) {
            $this->items[] = new Item($product, $quantity);
        }

        return $this;
    }

    /**
     * Return items array
     *
     * @return array
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * Get item
     *
     * @param integer $index
     * @return Item
     */
    public function getItem($index): Item
    {
        if (!isset($this->items[$index])) {
            throw new \OutOfBoundsException("Try to get value with index: " . $index);
        } else {
            return $this->items[$index];
        }
    }

    /**
     * @return float
     */
    public function getTotalPrice(): float
    {
        $sum = 0;
        foreach ($this->items as $key => $item) {
            $sum += $item->getProduct()->getPrice() * $item->getQuantity();
        }

        return floatval(number_format($sum, 2, ',', ''));
    }

    /**
     * @return float
     */
    public function getTotalPriceGross()
    {
        $sum = 0;
        /** @var Item $item */
        foreach ($this->items as $key => $item) {
            $sum += $item->getProduct()->getPriceGross() * $item->getQuantity();
        }

        return floatval(number_format($sum, 2, '.', ''));
    }

    /**
     * Remove product from cart
     *
     * @param Product $product
     * @throws Exception\QuantityTooLowException
     */
    public function removeProduct(Product $product): void
    {
        $items = $this->items;
        $this->items = [];

        /** @var Item $item */
        foreach ($items as $item) {
            $itemProduct = $item->getProduct();
            if ($itemProduct !== $product) {
                $this->addProduct($itemProduct, $item->getQuantity());
            }
        }
    }

    /**
     * Set quantity for product in cart
     *
     * @param Product $product
     * @param integer $quantity
     * @throws Exception\QuantityTooLowException
     */
    public function setQuantity(Product $product, $quantity): void
    {
        $this->removeProduct($product);
        $this->addProduct($product, $quantity);
    }

    /**
     * @param int $id
     * @return Order
     */
    public function checkout(int $id): Order
    {
        $order = new Order($id, clone $this);
        $this->items = [];

        return $order;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $array = [];

        /** @var Item $item */
        foreach ($this->items as $item) {
            $array[] = [
                'id' => $item->getProduct()->getId(),
                'quantity' => $item->getQuantity(),
                'total_price' => $item->getTotalPrice(),
                'total_price_gross' => $item->getTotalPriceGross(),
                'tax' => $item->getProduct()->getTaxPercentage(),
            ];
        }

        return $array;
    }
}
