<?php

namespace Recruitment\Cart;

use Recruitment\Entity\Product;
use Recruitment\Cart\Exception\QuantityTooLowException;

/**
 * Class Item
 * @package Recruitment\Cart
 */
class Item
{
    /** @var Product */
    private $product;

    /** @var int */
    private $quantity;

    /**
     * Item constructor.
     * @param Product $product
     * @param $quantity
     */
    public function __construct(Product $product, $quantity)
    {
        if ($product->getMinimumQuantity() > $quantity) {
            throw new \InvalidArgumentException();
        } else {
            $this->product = $product;
            $this->quantity = $quantity;
        }
    }

    /**
     * @return Product
     */
    public function getProduct(): Product
    {
        return $this->product;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @return float
     */
    public function getTotalPrice(): float
    {
        return $this->product->getPrice() * $this->quantity;
    }

    /**
     * @return float
     */
    public function getTotalPriceGross(): float
    {
        return $this->product->getPriceGross() * $this->quantity;
    }

    /**
     * @param int $quantity
     * @throws QuantityTooLowException
     */
    public function setQuantity(int $quantity): void
    {
        if ($this->product->getMinimumQuantity() > $quantity) {
            throw new QuantityTooLowException();
        } else {
            $this->quantity = $quantity;
        }
    }
}
